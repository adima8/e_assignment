# A mini-pipeline to perfom training and inference on given data

## Environment

To setup an environment you can use the following command

```conda env create -n <env_name> -f environment.yaml```
## Training
To perform train pipeline use the following command from **project root** 
   
   ```python src/cli.py train```
   
You can choose input data and algorithm in the cli parameters. Please see the following command for help

   ```python src/cli.py train --help```

You can setup other parameters in the `src/config.py` file. These include:
* `CAT_FEATURES, NUM_FEATURES` - specifies types of features in dataset. Each type is transformed accordingly
* `N_FOLDS` - number of folds used for cross-validation while tuning hyperparameters
* `ALGO_MAP` - a dictionary which allow to expand algorithms which can be used `python src/cli.py train`. The algorithms should be scikit-leaarn API compatible
* `RANDOM_STATE` - fixates random state during cross-validation split
* `PARAM_SPACE` - search space for hyperparameter tuning
* `SCORING_METRIC` - Scoring metric used to estimate the result of cross-validation. Currently supporting only scikit-learn predefined metrics - https://scikit-learn.org/stable/modules/model_evaluation.html 

The training output is written to `data/artifacts` directory and includes the following files:
* `model` - the pickle with model trained on full data with best hyperparameters
* `data_transformer` - the pickle of sckikit-learn data transformer pipeline used fit on train data, which can be used on test data
* `best_params` - json with best parameters as per model tuning algorithm
* `best_score` - json best score shown during model tuning algorithm

## Inference 
 Inference is performed via the following command:
 
 ```python src/cli.py inference```

 You can specify the data to make inference on and the name of the output file. By default the output is written to `data/inference/val.csv` file. You can check out the `--help` option of the command above for further information.

 In the beginning a simple check is performed to make sure all the necessary artifacts for inference are available.

 ## Further improvements
 This pipeline is light-weight and some things are coded for this specific task. Some further options to make it more universal and expandable:
  1. Make separate cli commands for feature transformation, hyper-parameter tuning and training
  2. Make `repro` pipeline in `dvc` using cli-commands from #1 as stages, so that we could benefit from ml-pipeline management functions provided by `dvc`
  3. Expand hyper-parameter tuning with RandomSearch, TPE-search methods, etc.
  4. Make a constructor of cli-commands which accepts user-defined functions (for example for data transformation) for each stage.


from multiprocessing import Pipe
import pandas as pd
import numpy as np
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import (StandardScaler, 
                                   OneHotEncoder, PolynomialFeatures)
from sklearn.pipeline import make_pipeline, Pipeline
from sklearn.compose import make_column_transformer
from typing import Tuple, Optional

from config import META_FEATURES, TARGET_FEATURE, NUM_FEATURES, CAT_FEATURES


def prepare_data(
    data: pd.DataFrame, 
    transformer:Optional[Pipeline]=None) -> Tuple[
                                                pd.DataFrame, 
                                                Optional[pd.Series], 
                                                pd.DataFrame, 
                                                Pipeline ]:
    """Prepares and trains data transformer for new data or fits existing 
        transformer for inference

    Args:
        data (pd.DataFrame): incoming raw data
        transformer (Optional[Pipeline], optional): applies transformer if 
            there is one. If None fits transformer to current data. 
            Defaults to None.

    Returns:
        Tuple[pd.DataFrame, Optional[pd.Series], pd.DataFrame, Pipeline ]: 
            Model features, model target, meta features and 
            fit (or passed) transformer
    """
    X = data.drop(META_FEATURES + [TARGET_FEATURE], axis=1, errors='ignore')
    y = data[TARGET_FEATURE] if TARGET_FEATURE in data.columns else None
    meta = data[META_FEATURES]
    
    if transformer is None:
        print("Fitting new transformer")
        num_pipeline = make_pipeline(
            SimpleImputer(strategy='constant', fill_value=0), 
            StandardScaler(),
            PolynomialFeatures(degree=2, interaction_only=True, 
                                include_bias=False)
        )
        
        transformer = make_column_transformer(
            (num_pipeline, NUM_FEATURES),
            (OneHotEncoder(), CAT_FEATURES),
            remainder='passthrough'
        )
        
        transformer.fit(X)
        
    X = pd.DataFrame(transformer.transform(X), 
                     columns=transformer.get_feature_names_out() )
    return (X, y, meta, transformer)


def postprocess_predictions(preds: np.ndarray) -> np.ndarray:
    """Postprocesses predictions (clips negative values to 0)

    Args:
        preds (np.ndarray): Incoming predictions

    Returns:
        np.ndarray: Post-processed predicitons
    """
    return np.clip(preds, 0, None)
    
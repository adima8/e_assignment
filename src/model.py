from typing import Tuple
from sklearn.model_selection import GridSearchCV, KFold
import pandas as pd
from config import N_FOLDS, RANDOM_STATE
from sklearn.base import BaseEstimator

def train_and_tune(
    X: pd.DataFrame, 
    y: pd.Series,
    param_grid: dict,
    algo:str, 
    alg_def_args: dict, 
    scoring_name: str) -> Tuple[dict, float, str, BaseEstimator]:
    """Fits a grid search using given parameters and returns result and 
        best model

    Args:
        X (pd.DataFrame): features
        y (pd.Series): target
        param_grid (dict): parameters grid
        algo (str): alogrithm alias from config
        alg_def_args (dict): default arguments of algorithm before tuning
        scoring_name (str): scoring metric name

    Returns:
        Tuple[dict, float, str, BaseEstimator]: best score, parameters 
        and model
    """
    print(f"random state : {RANDOM_STATE}")
    grid_search = GridSearchCV(
        algo(**alg_def_args), param_grid, 
        cv=KFold(N_FOLDS, random_state=RANDOM_STATE, shuffle=True),  
        scoring=scoring_name)

    grid_search.fit(X, y)
    best_params = grid_search.best_estimator_.get_params()
    best_score = grid_search.best_score_
    return best_params, best_score, scoring_name, grid_search
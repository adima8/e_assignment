from sklearn.linear_model import Ridge, Lasso
import os

## Feature variables

# features not used for prediction, but used to identify an entity of prediction
META_FEATURES = ['user']
# name of target feature
TARGET_FEATURE = 'revenue' 
# categorical features
CAT_FEATURES = ['region', 'device']
# numerical features
NUM_FEATURES = ['accounts_count', 'deposit_count', 'withdrawal_count', 
                'trade_sum_usd', 'trade_profit_usd', 'trades_count'] 


## Model variables

# number of folds used for cross-validation
N_FOLDS = 10

# random state used wherever it is possible
RANDOM_STATE = 9 

# map of compatible with sklearn classes and their aliases for cli
ALGO_MAP = {'ridge': Ridge, 'lasso': Lasso} 

# search space for tuning model
PARAM_SPACE = {'alpha': [1e-3, 1e-2, 1e-1, 1, 2, 3, 10, 20, 50, 100] } 

# default parameters of algorithm applied before tuning
ALGO_PARS_TUNING = {} 

# Scoring metric used to estimate the result of cross-validation.
# Currently supporting only scikit-learn predefined metrics
# https://scikit-learn.org/stable/modules/model_evaluation.html 
SCORING_METRIC = 'neg_mean_absolute_error' 

## Project directories

# directory for storing artifacts of the model
ARTIFACTS_DIR = 'data/artifacts'
# path for saving model
MODEL_DIR = os.path.join(ARTIFACTS_DIR, 'model')
# path for saving data transformer
TRANSFORMER_DIR = os.path.join(ARTIFACTS_DIR, 'data_transformer')


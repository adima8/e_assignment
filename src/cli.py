import click
import pandas as pd
import joblib
import os
import json
from typing import Tuple

from config import ALGO_MAP, PARAM_SPACE, ALGO_PARS_TUNING, \
    SCORING_METRIC, ARTIFACTS_DIR, MODEL_DIR, TRANSFORMER_DIR
from model import train_and_tune
from data import prepare_data, postprocess_predictions

pd.options.mode.chained_assignment = None  

@click.group()
def cli():
    pass


@cli.command('train')
@click.option('--data_path', 
    default='data/raw/train/train.csv',
    type=str, 
    help="path to raw train file",
    show_default=True)
@click.option('--algo', 
    default='ridge',
    help='Algorithm used for training',
    show_default=True
    )
def train_pipeline(data_path:str, algo:str) -> Tuple[str]:
    """CLI command to perform train pipeline

    Args:
        data_path (str): path to raw train data
        algo (str): algorithm alias (should be present in config.ALGO_MAP)

    Returns:
        Tuple[str]: path to trained model, path to trained data transformer
    """
    data = pd.read_csv(data_path)
    X_tr, y_tr, meta_tr, trnsfmr = prepare_data(data)
    if algo not in ALGO_MAP:
        raise ValueError("algo not in ALGO_MAP config variable")
    algo_sk = ALGO_MAP[algo]
    print("Training model")
    best_pars, best_score, scoring_metric, mdl = train_and_tune(
        X_tr, y_tr, PARAM_SPACE, 
        algo_sk, ALGO_PARS_TUNING, SCORING_METRIC)
    os.makedirs(ARTIFACTS_DIR, exist_ok=True)
    print("Logging metrics and saving model")
    with open(os.path.join(ARTIFACTS_DIR, 'best_score'), 'w') as f:
        json.dump({scoring_metric: best_score}, f)
    with open(os.path.join(ARTIFACTS_DIR, 'best_params'), 'w') as f:
        json.dump(best_pars, f)
    joblib.dump(mdl, MODEL_DIR)
    joblib.dump(trnsfmr, TRANSFORMER_DIR)
    return (MODEL_DIR, TRANSFORMER_DIR)


@cli.command('inference')
@click.option('--data_path', 
    default='data/raw/inference/val.csv',
    help="path to raw inference file",
    show_default=True)
@click.option('--name',
    help='inference run alias',
    default='val',
    show_default=True
    )
def inference_pipeline(data_path: str, name: str) -> str:
    """CLI command to perform inference

    Args:
        data_path (str): path to raw data on which to perform inference
        name (str): inference run alias (used to name inference results)

    Returns:
        str: path to inference results
    """
    # Asserting that we have a trained model and data transformer, 
    # this can be also done, using dvc repro 
    assert os.path.exists(TRANSFORMER_DIR),\
         "No trained data transformer found. Please train model first"
    assert os.path.exists(MODEL_DIR), \
        "No trained model found. Please train model first"
    os.makedirs('data/inference_result', exist_ok=True)
    data = pd.read_csv(data_path)
    mdl = joblib.load(MODEL_DIR)
    trnsfmr = joblib.load(TRANSFORMER_DIR)
    X, _, pred_df, _ = prepare_data(data, trnsfmr)
    preds = postprocess_predictions(mdl.predict(X))
    pred_df['prediction'] = preds
    res_path = os.path.join('data/inference_result', f"{name}.csv")
    pred_df.to_csv(res_path, index=False)
    print(f"Inference results are saved to {res_path}")
    return res_path


if __name__ == '__main__':
    cli()